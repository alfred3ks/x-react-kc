import axios from 'axios';

// Creamos un cliente de axios:
const client = axios.create({
  baseURL: import.meta.env.VITE_API_BASE_URL,
});

// Creamos un interceptor de la respuesta y guardarlos en el objeto config de axios:
client.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    if (error.response) {
      // 400/500 server error
      return Promise.reject({
        message: error.response.statusText,
        ...error.response,
        ...error.response.data,
      });
    }
    // error network
    return Promise.reject({ message: error.message });
  }
);

export const setAuthorizationHeader = (token) => {
  return (client.defaults.headers.common['Authorization'] = `Bearer ${token}`);
};

// Método para quitar de axios el token:
export const removeAuthorizationHeader = () => {
  delete client.defaults.headers.common['Authorization'];
};

export default client;
