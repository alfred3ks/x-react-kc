import styled from 'styled-components';

const accentColor = '#007bff';

const Button = styled.button`
  padding: 5px 10px;
  background-color: ${(props) =>
    props.$variant === 'primary' ? accentColor : 'black'};
  color: #ffffff;
  border: none;
  border-radius: 3px;
  font-size: 1.6rem;
  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
  cursor: pointer;
  pointer-events: ${(props) => (props.disabled ? 'none' : 'auto')};
  text-decoration: none;
  margin-right: 10px;
  &:hover {
    background-color: #0056b3;
  }
`;

export default Button;
