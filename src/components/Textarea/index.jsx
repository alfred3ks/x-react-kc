import styles from './Textarea.module.css';

const TextArea = ({ className, ...props }) => {
  return (
    <div className={className}>
      <textarea
        className={styles['textarea-input']}
        name="textarea"
        id="textarea"
        cols="30"
        rows="10"
        {...props}
      ></textarea>
    </div>
  );
};

export default TextArea;
