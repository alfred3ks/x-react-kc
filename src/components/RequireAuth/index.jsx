import { useAuth } from '../../pages/auth/context';
import { Navigate, useLocation } from 'react-router-dom';

const RequireAuth = ({ children }) => {
  const { isLogged } = useAuth();

  const location = useLocation();

  return isLogged ? (
    children
  ) : (
    <Navigate to={'/login'} state={{ from: location }} />
  );
};

export default RequireAuth;
