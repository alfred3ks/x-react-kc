import Footer from '../Footer';
import Header from '../Header';
import { Outlet } from 'react-router-dom';

import styles from './Layout.module.css';

const Layout = ({ children }) => {
  return (
    <div className={styles.container}>
      <Header />
      <main>{children ? children : <Outlet />}</main>
      <Footer>@alfred3ks</Footer>
    </div>
  );
};

export default Layout;
