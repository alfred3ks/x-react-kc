const Content = ({ title, children, className }) => {
  return (
    <div className={className}>
      <h2>{title}</h2>
      {children}
    </div>
  );
};

export default Content;
