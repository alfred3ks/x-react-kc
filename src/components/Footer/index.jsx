import styles from './Footer.module.css';

const Footer = ({ children }) => {
  const currentYear = new Date().getFullYear();
  return (
    <footer className={styles.footer}>
      @{currentYear} {children} Inc. All rights reserved.
    </footer>
  );
};

export default Footer;
