import Logo from '../Logo';

import { Link, NavLink } from 'react-router-dom';

import styles from './Header.module.css';
import AuthButton from '../AuthButton';

const Header = () => {
  return (
    <header className={styles.header}>
      <Link to={'/'}>
        <Logo className={styles.logo} />
      </Link>
      <nav className={styles.nav}>
        <NavLink className={styles.navlink} to={'/tweets/new'}>
          Create tweet
        </NavLink>
        <NavLink className={styles.navlink} to={'/tweets'} end>
          Latest tweest
        </NavLink>
        <AuthButton className={styles.confirmation} />
      </nav>
    </header>
  );
};

export default Header;
