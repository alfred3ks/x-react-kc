import styles from './Imput.module.css';

const Input = ({ id, name, type, value, onChange }) => {
  return (
    <div className={styles.container}>
      <label htmlFor="">{name}</label>
      <input
        id={id}
        type={type}
        name={name}
        onChange={onChange}
        value={value}
      />
    </div>
  );
};

export default Input;
