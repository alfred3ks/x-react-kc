import styles from './NotFount.module.css'; // Importamos los estilos CSS

const NotFound = () => {
  return (
    <div className={styles.notFoundContainer}>
      <h1 className={styles.notFoundHeading}>404 - Página no encontrada</h1>
      <p className={styles.notFoundText}>
        Lo sentimos, la página que estás buscando no existe.
      </p>
    </div>
  );
};

export default NotFound;
