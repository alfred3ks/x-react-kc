import { useState } from 'react';
import { useAuth } from '../../pages/auth/context';
import { logout } from '../../pages/auth/service';
import Button from '../Button';

import { Link } from 'react-router-dom';

const AuthButton = ({ className }) => {
  const [showConfirmation, setShowConfirmation] = useState(false);

  // Recibimos del contexto isLogged y onLogout:
  const { isLogged, onLogout } = useAuth();

  const handleLogout = () => {
    setShowConfirmation(true);
  };

  const handleConfirmLogout = async () => {
    // Borramos el token de axios:
    await logout();
    // Eliminamos token:
    onLogout();
    setShowConfirmation(false);
  };

  const handleCancelLogout = () => {
    setShowConfirmation(false);
  };

  return (
    <>
      {isLogged ? (
        <>
          {!showConfirmation && <Button onClick={handleLogout}>Log out</Button>}
          {showConfirmation && (
            <div className={className}>
              <p>Do you want to logout?</p>
              <Button onClick={handleConfirmLogout}>Yes</Button>
              <Button $variant={'primary'} onClick={handleCancelLogout}>
                Cancel
              </Button>
            </div>
          )}
        </>
      ) : (
        <Button as={Link} to={'/login'} $variant={'primary'}>
          Log in
        </Button>
      )}
    </>
  );
};

export default AuthButton;
