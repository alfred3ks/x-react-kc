import { Routes, Route, Navigate } from 'react-router-dom';
import Layout from './components/Layout';
import TweetsPage from './pages/tweets/TweetsPage';
import LoginPage from './pages/auth/LoginPage';
import TweetPage from './pages/tweets/TweetPage';
import NewTweetPage from './pages/tweets/NewTweetPage';
import NotFound from './components/NotFount';
import RequireAuth from './components/RequireAuth';

import './styles/reset.css';

const App = () => {
  return (
    <Routes>
      <Route path="/login" element={<LoginPage />} />
      <Route path="/" element={<Navigate to="/tweets" />} />
      {/* Rutas anidadas */}
      <Route path="/tweets" element={<Layout />}>
        <Route index element={<TweetsPage />} />
        <Route path=":id" element={<TweetPage />} />
        <Route
          path="new"
          element={
            <RequireAuth>
              <NewTweetPage />
            </RequireAuth>
          }
        />
      </Route>

      {/* Pagina 404 */}
      <Route path="*" element={<Navigate to="/404" />} />
      <Route
        path="/404"
        element={
          <Layout>
            <NotFound />
          </Layout>
        }
      />
    </Routes>
  );
};

export default App;
