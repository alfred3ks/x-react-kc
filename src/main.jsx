import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import { setAuthorizationHeader } from './api/client.js';
import storage from './utils/storage.js';
import AuthContextProvider from './pages/auth/AuthContextProvider';
import { BrowserRouter } from 'react-router-dom';

const accedToke = storage.get('auth');
if (accedToke) {
  // Guardamos en axios el token
  setAuthorizationHeader(accedToke);
}

const isUserLogged = !!accedToke; // true o false

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <AuthContextProvider initiallyLogged={isUserLogged}>
        <App />
      </AuthContextProvider>
    </BrowserRouter>
  </React.StrictMode>
);
