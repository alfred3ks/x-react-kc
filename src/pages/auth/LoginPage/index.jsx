import { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import { login } from '../service';
import { useAuth } from '../context';

import styles from './LoginPage.module.css';

const LoginPage = () => {
  // Recibimos del contexto onLogin con el custom hook:
  const { onLogin } = useAuth();

  // Creamos estados para los inputs:
  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  });

  const [error, setError] = useState(null);
  const [isFeching, setIsFeching] = useState(false);

  // accedemos al objeto location de react router:
  const location = useLocation();
  // Usamos el hook para navegar:
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      setIsFeching(true);
      // Llamamos el método de hacer login:
      await login(credentials);
      setIsFeching(false);

      // Disparamos la funcion de logger:
      onLogin();

      // Si tenemos algo en location redireccionamos sino al home:
      const to = location?.state?.from?.pathname || '/';
      navigate(to, { replace: true });
    } catch (error) {
      setIsFeching(false);
      setError(error);

      setTimeout(() => {
        setError(null);
      }, 5000);
    }
  };

  const handleChange = (e) => {
    setCredentials((currentCredentials) => ({
      ...currentCredentials,
      [e.target.name]: e.target.value,
    }));
  };

  const { username, password } = credentials;
  const disabled = !(username && password) || isFeching;

  return (
    <div className={styles.container}>
      <h1>Login into X</h1>
      <form onSubmit={handleSubmit}>
        <Input
          type="text"
          id="username"
          name="username"
          onChange={handleChange}
          value={username}
          className={styles.input}
        />
        <Input
          type="password"
          id="password"
          name="password"
          onChange={handleChange}
          value={password}
          className={styles.input}
        />
        <Button $variant={'primary'} disabled={disabled}>
          {isFeching ? 'Loading' : 'Login'}
        </Button>
        {error && <div className={styles.error}>{error.message}</div>}
      </form>
    </div>
  );
};

export default LoginPage;
