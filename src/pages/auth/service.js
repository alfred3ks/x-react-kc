import client, {
  removeAuthorizationHeader,
  setAuthorizationHeader,
} from '../../api/client';
import storage from '../../utils/storage';

// Url de la consulta:
const url = '/auth/login';

// Creo el método para hacer la peticion a la api y mandar las credenciales del user:
export const login = async (credentials) => {
  const response = await client.post(url, credentials);
  const { accessToken } = response;
  // Guadamos el token en la cabecera de axios ojo en memoria:
  setAuthorizationHeader(accessToken);

  // Guardamos el token en localStorage:
  storage.set('auth', accessToken);

  return response;
};

// Método para hacer logout:
export const logout = async () => {
  try {
    // Quitamos el token de axios
    removeAuthorizationHeader();
    storage.remove('auth');
  } catch (error) {
    // Maneja cualquier error que pueda ocurrir durante la ejecución
    console.error('Error al cerrar sesión:', error);
  }
};
