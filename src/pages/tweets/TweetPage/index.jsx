import { useEffect, useState } from 'react';
import Content from '../../../components/Content';
import { useParams, useNavigate } from 'react-router-dom';

import { getTweet } from '../service';

const TweetPage = () => {
  const [tweet, setTweet] = useState(null);
  const navigate = useNavigate();

  const { id } = useParams();

  useEffect(() => {
    const getTweetApi = async () => {
      try {
        const tweetApi = await getTweet(id);
        setTweet(tweetApi);
      } catch (error) {
        if (error.status === 404) {
          navigate('/404');
        }
      }
    };
    getTweetApi();
  }, [id, navigate]);

  return (
    <Content title={'Tweet detail'}>
      <div>
        <p>Tweet details {id} goes here...</p>
        <div>{tweet && <code>{JSON.stringify(tweet)}</code>}</div>
      </div>
    </Content>
  );
};

export default TweetPage;
