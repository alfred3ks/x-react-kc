import { useEffect, useState } from 'react';
import { getLatestTweets } from '../service/';
import Button from '../../../components/Button';
import { Link } from 'react-router-dom';
import Content from '../../../components/Content';

import styles from './TweetsPage.module.css';

const EmptyList = () => (
  <div className={styles.emptyList}>
    <p>No tweets yet!</p>
    <Button $variant="primary">Create tweet</Button>
  </div>
);

const TweetsPage = () => {
  const [tweets, setTweets] = useState([]);

  useEffect(() => {
    const fetchTweets = async () => {
      const data = await getLatestTweets();
      setTweets(data);
      // setTweets([]);
    };
    fetchTweets();
  }, []);

  //para saber si hay informacion en la bd que mostrar, con el !! lo pasamos a booleano:
  const hasTweets = !!tweets.length;

  return (
    <Content title="What's going on..." className={styles.content}>
      <div className={styles.container}>
        {hasTweets ? (
          <ul>
            {tweets.map((tweet) => {
              return (
                <li key={tweet.id} className={styles.tweetLink}>
                  <Link to={`${tweet.id}`}>{tweet.content}</Link>
                </li>
              );
            })}
          </ul>
        ) : (
          <EmptyList />
        )}
      </div>
    </Content>
  );
};

export default TweetsPage;
