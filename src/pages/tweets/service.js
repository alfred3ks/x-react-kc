// Importamos el client desde la api:
import client from '../../api/client';

// Este es el endpoint para consulta get:
const urlApi = '/api/tweets';

// Creamos el método para hacer la peticion get de los tweets:
export const getLatestTweets = () => {
  return client.get(
    `${urlApi}?_expand=user&_embed=likes&_sort=updatedAt&_order=desc`
  );
};

// Método para solicitar tweet página detalle:
export const getTweet = (tweetId) => {
  const url = `${urlApi}/${tweetId}`;
  // Hacemos una peticion:
  return client.get(url);
};

// Método para crear un nuevo tweet:
export const createTweet = (tweet) => {
  const url = urlApi;
  return client.post(url, tweet);
};
