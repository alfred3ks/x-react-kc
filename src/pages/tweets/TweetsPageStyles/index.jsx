// import './TweetsPage.css';
import styles from './TweetsPage.module.css';

const TweetsPageStyles = ({ dark }) => {
  const classTitle = `${styles.title} ${dark ? styles.dark : styles.light}`;
  return <h1 className={classTitle}>TweetsPage Styles</h1>;
};

export default TweetsPageStyles;
